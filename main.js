// MODULE 1 :

// Importer chaque élément un par un => "import { nom de l'élément } from "chemin vers module""

import { phrase } from "./module1.js";
import { rale } from "./module1.js";
import { unNumero } from "./module1.js";
import { unAutreNumero } from "./module1.js";



// Importer la liste des éléments qu'on veut d'un seul coup => 
// "import { noms des éléments séparés par des virgules } from "chemin vers module""

import { phrase, rale, unNumero, unAutreNumero } from "./module1.js";



//utilisation des éléments exportés dans les deux premiers cas

console.log(phrase);
console.log(rale);
let calcul = unNumero + unAutreNumero;
console.log(calcul);

// L'utilisation des éléments exportés se fait ici avec leurs noms d'origine, 
// aussi naturellement que s'ils avaient été déclarés dans ce script (main.js)



// Importer chaque élément un par un en le renommant 
// => "import { nom de l'élément as nouveau nom de l'élément} from "chemin vers module""

import { phrase as bla } from "./module1.js";
import { rale as gna } from "./module1.js";
import { unNumero as uN } from "./module1.js";
import { unAutreNumero as uAN } from "./module1.js";



// Importer la liste des éléments qu'on veut d'un seul coup et en les renommant => 
// "import { noms des éléments as nouveaux noms des éléments, séparés par des virgules } from "chemin vers module""

import { phrase as bla, rale as gna, unNumero as uN, unAutreNumero as uAN } from "./module1.js";



//utilisation des éléments exportés dans les deux cas où ils ont été renommés

console.log(bla);
console.log(gna);
let calcul = uN + uAN;
console.log(calcul);

// L'utilisation des éléments exportés se fait ici avec leurs nouveaux noms, 
// aussi naturellement que s'ils avaient été déclarés sous ces nouveaux noms dans ce script (main.js)
// cela peut être avantageux si on veut raccourcir les noms ou que leur nom d'origine peut porter à confusion dans le nouveau script



// On peut importer tout le mode d'un seul coup => "import * as nom donné au module from "chemin vers le module""

import * as chiffresEtLettres from "./module.1.js";



// Utilisation des éléments exportés en mode tous *

console.log(chiffresEtLettres.phrase);
console.log(chiffresEtLettres.rale);
let calcul = chiffresEtLettres.unNumero + chiffresEtLettres.unAutreNumero;
console.log(calcul);



// MODULE 2 :

// Importer chaque élément un par un => "import { nom de l'élément } from "chemin vers module""

import { alAncienne } from "./module2.js";
import { saluer } from "./module2.js";
import { parler } from "./module2.js";
import { Personne } from "./module2.js";



// Importer la liste des éléments qu'on veut d'un seul coup => 
// "import { noms des éléments séparés par des virgules } from "chemin vers module""

import { alAncienne, saluer, parler, Personne } from "./module2.js";



//utilisation des éléments exportés dans les deux premiers cas

alAncienne();
saluer("Madame");
parler("Aurélien");
let orel = new Personne("Aurélien", "indéfini");

// L'utilisation des éléments exportés se fait ici avec leurs noms d'origine, 
// aussi naturellement que s'ils avaient été déclarés dans ce script (main.js)


// Importer chaque élément un par un en le renommant 
// => "import { nom de l'élément as nouveau nom de l'élément} from "chemin vers module""

import { alAncienne as alA } from "./module2.js";
import { saluer as hi } from "./module2.js";
import { parler as talk} from "./module2.js";
import { Personne as SomeOne} from "./module2.js";



// Importer la liste des éléments qu'on veut d'un seul coup et en les renommant => 
// "import { noms des éléments as nouveaux noms des éléments, séparés par des virgules } from "chemin vers module""

import { alAncienne as alA, saluer as hi, parler as talk, Personne as SomeOne } from "./module2.js";



//utilisation des éléments exportés dans les deux cas où ils ont été renommés

alA();
hi("Madame");
talk("Aurélien");
let orel = new SomeOne("Aurélien", "indéfini");

// L'utilisation des éléments exportés se fait ici avec leurs nouveaux noms, 
// aussi naturellement que s'ils avaient été déclarés sous ces nouveaux noms dans ce script (main.js)
// cela peut être avantageux si on veut raccourcir les noms ou que leur nom d'origine peut porter à confusion dans le nouveau script



// On peut importer tout le mode d'un seul coup => "import * as nom donné au module from "chemin vers le module""

import * as parlotte from "./module.2.js";



// Utilisation des éléments exportés en mode tous *

parlotte.alAncienne();
parlotte.saluer('Madame');
parlotte.parler('Aurélien');
let orel = new parlotte.Personne("Aurélien", "indéfini");



// MODULE 3 :

// Importer chaque élément un par un => "import { nom de l'élément } from "chemin vers module""

import { addition } from "./module3.js";
import { soustraction } from "./module3.js";
import { multiplication } from "./module3.js";
import { division } from "./module3.js";



// Importer la liste des éléments qu'on veut d'un seul coup => 
// "import { noms des éléments séparés par des virgules } from "chemin vers module""

import { addition, soustraction, multiplication, division } from "./module3.js";



//utilisation des éléments exportés dans les deux premiers cas

addition(3,6);
soustraction(5,3);
multiplication(9,4);
division(8,2);

// L'utilisation des éléments exportés se fait ici avec leurs noms d'origine, 
// aussi naturellement que s'ils avaient été déclarés dans ce script (main.js)



// Importer chaque élément un par un en le renommant 
// => "import { nom de l'élément as nouveau nom de l'élément} from "chemin vers module""

import { addition as plus } from "./module3.js";
import { soustraction as moins } from "./module3.js";
import { multiplication as fois} from "./module3.js";
import { division as par} from "./module3.js";



// Importer la liste des éléments qu'on veut d'un seul coup et en les renommant => 
// "import { noms des éléments as nouveaux noms des éléments, séparés par des virgules } from "chemin vers module""

import { addition as plus, soustraction as moins, multiplication as fois, division as par } from "./module3.js";



//utilisation des éléments exportés dans les deux cas où ils ont été renommés

plus(3,6);
moins(5,3);
fois(9,4);
par(8,2);

// L'utilisation des éléments exportés se fait ici avec leurs nouveaux noms, 
// aussi naturellement que s'ils avaient été déclarés sous ces nouveaux noms dans ce script (main.js)
// cela peut être avantageux si on veut raccourcir les noms ou que leur nom d'origine peut porter à confusion dans le nouveau script



// On peut importer tout le mode d'un seul coup => "import * as nom donné au module from "chemin vers le module""

import * as calculette from "./module.3.js";



// Utilisation des éléments exportés en mode tous *

calculette.addition(3,6);
calculette.soustraction(5,3);
calculette.multiplication(9,4);
calculette.division(8,2);