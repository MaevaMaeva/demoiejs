// Déclarations classiques, sans exports pour faire les exports APRES

const phrase = "Blablabla";

const rale = "Gnagnagna";

let unNumero = 43;

let unAutreNumero = 12;



// Exporter en individuel, au fur et à mesure qu'on déclare => "export QQCH"

export const phrase = "Blablabla";

export const rale = "Gnagnagna";

export let unNumero = 43;

export let unAutreNumero = 12;



// Exporter tous les éléments de même type d'un seul coup =>

export const phrase, rale;

export let unNumero, unAutreNumero;



// Exporter un élément après l'avoir déclaré =>

export { phrase };



// Exporter une liste après déclaration, peu importe de quels types il s'agit 
// => "export {noms des éléments à exporter séparés par des virgules}"

export { phrase, rale, unNumero, unAutreNumero };



// Exporter en renommant l'élément => "export { nom de l'élément as nouveau nom de l'élément}"

export { phrase as bla };



// Exporter une liste renommée =>
export { phrase as bla, rale as gna, unNumero as uN, unAutreNumero as uAN };
