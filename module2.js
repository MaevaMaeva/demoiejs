// Déclarations classiques, sans exports pour faire les exports APRES

function alAncienne (){
    console.log('toto dit bonjour');
}

const saluer = qqn => console.log ('Bonjour, bonjour '+ qqn );

const parler = prenom => console.log(prenom + ' fait un monologue.');

class Personne { 
    constructor(name, age){
        this.name = name;
        this.age = age;
}
race = "humain";
}


// Exporter en individuel, au fur et à mesure qu'on déclare => "export QQCH"

export function alAncienne (){
    console.log('toto dit bonjour');
}

export const saluer = qqn => console.log ('Bonjour, bonjour '+ qqn );

export const parler = prenom => console.log(prenom + ' fait un monologue.');

export class Personne { 
    constructor(name, age){
        this.name = name;
        this.age = age;
}
race = "humain";
}



// Exporter tous les éléments de même type d'un seul coup =>

export const saluer , parler;



// Exporter un élément après l'avoir déclaré => "export { nom de l'élément }"

export { alAncienne };



// Exporter une liste après déclaration, peu importe de quels types il s'agit 
// => "export {noms des éléments à exporter séparés par des virgules}"

export { alAncienne, saluer, parler, Personne };



// Exporter en renommant l'élément => "export { nom de l'élément as nouveau nom de l'élément}"

export { Personne as SomeOne };



// Exporter une liste renommée =>
export { alAncienne as alA, saluer as hi, parler as talk, Personne as SomeOne };