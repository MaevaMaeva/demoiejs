// Déclarations classiques, sans exports pour faire les exports APRES

const addition = (nbr1, nbr2) => console.log(nbr1 + nbr2);

const soustraction = (nbr1, nbr2) => console.log(nbr1 - nbr2);

const multiplication = (nbr1,nbr2)=> console.log(nbr1 * nbr2);

const division = (nbr1, nbr2)=> console.log(nbr1 / nbr2);



// Exporter en individuel, au fur et à mesure qu'on déclare => "export QQCH"

export const addition = (nbr1, nbr2) => console.log(nbr1 + nbr2);

export const soustraction = (nbr1, nbr2) => console.log(nbr1 - nbr2);

export const multiplication = (nbr1,nbr2)=> console.log(nbr1 * nbr2);

export const division = (nbr1, nbr2)=> console.log(nbr1 / nbr2);



// Exporter tous les éléments de même type d'un seul coup =>

export const addition, soustraction, multiplication, division;



// Exporter un élément après l'avoir déclaré =>

export { addition };



// Exporter une liste après déclaration, peu importe de quels types il s'agit 
// => "export {noms des éléments à exporter séparés par des virgules}"

export { addition, soustraction, multiplication, division };



// Exporter en renommant l'élément => "export { nom de l'élément as nouveau nom de l'élément}"

export { addition as plus };



// Exporter une liste renommée =>
export { addition as plus, soustraction as moins, multiplication as fois, division as par };